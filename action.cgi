#!/usr/bin/perl

use utf8;

#フォームから1行読み込み
read (STDIN, $PostData, $ENV{'CONTENT_LENGTH'});

#投稿時間
my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);
$year += 1900;
$mon += 1;

#IDを取得
open (IDIN , "<id.data");
	$id = <IDIN>;
	$id = $id + 0;
close(IDIN);

$id += 1;

open (IDOUT , ">id.data");
	print IDOUT $id;
close (IDOUT);

#読み込んだフォームからの1行を整形
foreach ( split(/&/, $PostData) ) {
	($key, $val) = split(/=/);
	$in{$key} = $val;
}

open (DATA, ">>bbs.log");
	print DATA "$id,";
	print DATA $in{"namae"};
	print DATA ",";
	print DATA $in{"body"};
	print DATA ",$year-$mon-$mday $hour:$min:$sec";
	print DATA "\n";
close(DATA);

#Redirection
print "location: index.cgi\n\n";
