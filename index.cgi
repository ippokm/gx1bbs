#!/usr/bin/perl

use utf8;
binmode (STDOUT,":utf8");

print "Content-Type: text/html; charset=UTF-8\n\n";

#HTML
print "<html>";
print << "HEADER";
	<head>
		<title>GX BBS kazuho</title>
		<link rel="stylesheet" type="text/css" href="all.css" media="all">	
	</head>
HEADER

print "<body>";

#header読み込み
&head;

print "<div class=\"container\">";

#form読み込み
&form;

print "<div id=\"contents\" class=\"contents\" >";


#bbs.logをハッシュの配列で取得
open (DATA, "<bbs.log");
		my $posts = [];
		while (my $line = <DATA>){
			chomp $line;
			
			my @recode = split(',', $line);
				#文字化け解除
				for ($i = 1; $i<=2; $i++){
						$recode[$i] =~ tr/+/ /;
						$recode[$i] =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack('H2', $1)/eg;	#URI ESCAPEを処理
						utf8::decode($recode[$i]);
				}	
			my $post = {};
			$post->{id}	=$recode[0];
			$post->{namae} =$recode[1];
			$post->{body} = $recode[2];
			$post->{timestamp} = $recode[3];

			push @$posts, $post;
		}	
close(DATA);

#投稿の表示
@$posts =reverse(@$posts);
foreach my $post (@$posts){
		print "$post->{id}:$post->{body}<Br>";
}

print "<br>";
print "</div>";
print "</div>";
print "</body>";
print "</html>";

#html header
sub head{
print << "HTML";
<div class="header_bar"><p class="title"><b>掲示板</b></p></div>

HTML
}

sub form{
print << "HTML";
<div class="form">
	<form action="action.cgi" method="post">
		name:<input type="text" name="namae" class="namae">
		body:<input type="text" name="body" class="body">
	<input type="submit" class="submit" value="send">
	</form>
</div>

HTML

}
